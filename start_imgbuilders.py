#!/usr/bin/env python3

import argparse, logging, json, subprocess, shlex, time
import libABCD


# parse command line arguments (config file)
parser = argparse.ArgumentParser(description='Start the image builders.')
parser.add_argument('jsonfile', default='experiment/img_builders.json', 
                    nargs='?',
                    help='the JSON file with builders information')
parser.add_argument('--expconfig', default='experiment/default.ini',
                    help='the experiment configuration file')
args = parser.parse_args()


# initialize libABCD to get list of active clients
libABCD.init('start_builders', expconfig=args.expconfig, listener=True,
              loglevel=logging.WARNING)

active_builders = []

def status_callback(msg: dict, topic: str):
    
    client_id = topic.rpartition('/')[2]

    if 'image_builder.' not in client_id: return
    if 'listener' in client_id: return

    if msg['payload'] == 'on':
        active_builders.append(client_id)

    # elif client_id in active_clients:
    #     active_clients.remove(client_id)

libABCD.add_callback('status/#', status_callback)


# wait a bit to get all the clients
time.sleep(.5)

if len(active_builders):
    print('>>> Already active builders: ')
    for c in active_builders: print(c)


# load the user-defined client list
with open(args.jsonfile, 'r') as f:
    builder_dict = json.load(f)

# start ccd clients
for cname, cdict in builder_dict.items():

    builder_name = f'image_builder.{cname}'

    if builder_name in active_builders:
        print(f'> {cname} already initialized.')
        continue

    sshargs = ''
    screenargs = f'screen -dmS {builder_name}'
    daqpath = ''

    if 'host' in cdict:
        if 'host' != 'localhost':
            if 'user' not in cdict:
                print(f'ERROR - "user" not defined for client {cname}')
                continue
            sshargs = f'ssh {cdict["user"]}@{cdict["host"]}'
            
            if 'script' not in cdict:
                if 'daqpath' in cdict:
                    daqpath = cdict["daqpath"]
                else:
                    print(f'ERROR - either "daqpath" or "script" should be defined for client {cname}')
                    continue

    if 'script' in cdict:
        execargs = cdict["script"]
        optionargs = ''

    else:
        execargs = 'source/image_builder.py'
        optionargs = f'--imbid {cname} '

        for key, value in cdict.items():

            if key in ['user', 'host', 'daqpath']: continue

            if key in ['expconfig',]: 
                optionargs += f'--{key} {daqpath}{value} '
                continue

            if key in ['dec_kwds']:
                optionargs += f"--{key} '{json.dumps(value)}'"
                continue

            optionargs += f'--{key} {value} '

    scmd = f'{sshargs} {screenargs} ./{daqpath}{execargs} {optionargs}'
    print(f'Executing: {scmd}')
    subprocess.Popen(shlex.split(scmd))
