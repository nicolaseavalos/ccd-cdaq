# Changelog

Released versions of ccd-cdaq

## [1.6] - 2024-05-10

### Added
- python environment now packaged with CDAQ
- --force option to `shutdown_ccd.py`

### Changed
- Updated README with new installation and better usage instructions


## [1.5] - 2024-03-27

### Added
- pyLTA option to ccd_client daq

### Changed
- logging changes to match libABCD 2.0

### Removed
- printstatus.py (obsolete)


## [1.4.2] - 2024-02-19

### Changed
- bugfix in `run_control`


## [1.4.1] - 2023-12-04

### Removed
- `libLTA` and `libLeach` no longer packaged with the CDAQ


## [1.4] - 2023-10-04

### Changed
- `mon_status.py` and `ccdmanager.py` dump variables to json
- `ccdmanager.py` and `run_control.py` reworked logic
- `scripts/example.py` reflects more common usage
- helper scripts adapted to new MQTT topics
- `start_daq.sh` no longer includes image builders

### Removed
- multiple queues support (buggy & unused)


## [1.3] - 2023-09-07

### Added

- `start_daq.sh` script that starts the tmux interface and the daq
- `image_builder` app that runs automatically after an image is finished
by a CCD client
- `FakeDecoder` for tests

### Changed

- `fakedaq` module moved inside `libFake`


## [1.2] - 2023-07-11

### Added

- `purge_current` function to RunDAQQueue and `purgecurrent` option to 
sendqman
- `script` is available as an option to `ccd_client.json`
- `pausequeue.py` script to pause a queue
- `mqttcleanretained(topic)` function to `_utilities.py`

### Changed

- Created `experiment/` directory for experiment-specific configuration
- expconfig defaults to `experiment/default.ini`
- helper scripts now print to console what they are publishing
- queues are now paused if a command targets a non-tracked client
- `delqueue.py` now accepts options `-t` and `-n`
- `Spy` starts with `--ignore-pings` option as default
- Better logging in source apps

### Removed
- `printstatus.py` (obsolete)


## [1.1] - 2023-06-15

### Added

- `shutdown_client` and `lift_emergency_stop` helper scripts and features
- better handling of conflicts between queues and busy clients
- libLeach
- "pause/unpause" features to queues

### Changed

- CCD clients' status is now more detailed
- DEBUG messages in file logs


### Removed

- message acknowledgment system
