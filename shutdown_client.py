#!/usr/bin/env python3

import argparse, sys, time
import libABCD

# parse command line arguments
parser = argparse.ArgumentParser(description='Shut down a CCD client.')
parser.add_argument('ccdid', help='the CCD client')
parser.add_argument('--expconfig', default='experiment/default.ini',
                    help='the experiment configuration file')
parser.add_argument('-f', '--force', action='store_true')
parser.add_argument('-s', '--stop', action='store_true')
args = parser.parse_args()


def exit_clean():
    libABCD.disconnect()
    sys.exit()

libABCD.init('shutdown_client', listener=True, expconfig=args.expconfig)

if not args.force:
    
    user_input = input(f'>>> Are you sure you want to shut down {args.ccdid}? (Yes/[no])')
    if user_input not in ['yes', 'y', 'Y']:
        print('Aborting.')
        exit_clean()


topic = f'ccd_clients/{args.ccdid}/shutdown'
if args.stop:
    topic = f'ccd_clients/{args.ccdid}/stop'
print(f'> Publishing to {topic}"')

libABCD.publish(topic, "")

libABCD.disconnect()
