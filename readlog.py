#!/usr/bin/env python3
from dateutil import parser, tz
from termcolor import colored
import argparse
import re

def parse_arguments():
    """Parse command-line arguments."""
    parser = argparse.ArgumentParser(description="Log file processor")
    parser.add_argument("filename", help="log file to open")
    parser.add_argument("--utc", "-u", action="store_true", help="print UTC time (default local time)")
    parser.add_argument("--noinfo", "-w", action="store_true", help="don't print info level messages")
    parser.add_argument("--debug", "-d", action="store_true", help="print debug messages")
    parser.add_argument("--cdaqdebug", "-c", action="store_true", help="print even daq debug messages (pongs)")
    parser.add_argument("--reverse", "-r", action="store_true", help="reverse log level printout (background color)")
    return parser.parse_args()

def convert_time(time_str, to_zone):
    """Convert time to a different timezone."""
    from_zone = tz.tzutc()
    utc = parser.parse(time_str)
    utc = utc.replace(tzinfo=from_zone)
    return utc.astimezone(to_zone)

def get_log_level_color(level):
    """Get the color for a log level."""
    colors = {
        "INFO": "green",
        "WARNING": "yellow",
        "ERROR": "red",
        "CRITICAL": "red"
    }
    return colors.get(level.strip())

def process_log_file(filename, args):
    """Process the log file and print the contents."""
    timestamp_pattern = re.compile(r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{1,3}')
    buffer = []
    with open(filename, 'r') as fp:
        for line in fp:
            # Check if this line starts a new log message
            if timestamp_pattern.match(line):
                # Flush the buffer and process the previous message
                if buffer:
                    process_message(buffer, args)
                    buffer = []
            # Add the line to the buffer
            buffer.append(line)
        # Process the last message in the buffer
        if buffer:
            process_message(buffer, args)

def process_message(buffer, args):
    """Process a log message and print the contents."""
    lines = [line.strip() for line in buffer]
    first_line = lines[0]
    parts = first_line.split(" - ")
    time_str = parts[0]
    who = parts[1]
    level = parts[2]
    content = " ".join(parts[3:]).strip()
    content += "\n" + "\n".join(lines[1:])
    if level.strip() == "DEBUG" and not args.debug:
        return
    if level.strip() == "INFO" and args.noinfo:
        return
    if content.startswith("Publishing") and not args.cdaqdebug:
        return
    time = convert_time(time_str, tz.tzutc() if args.utc else tz.tzlocal())
    color = get_log_level_color(level)
    attrs = ["reverse"] if args.reverse else []
    if level=="CRITICAL":
        attrs.append("bold")
    if color:
        text = colored(f"{level.strip()}@{who}", color, attrs=attrs)
    else:
        text = f"{level.strip()}@{who}"
    print(f"{time:%b%d %H:%M:%S} {text} {content.strip()}")

def main():
    args = parse_arguments()
    if args.cdaqdebug:
        args.debug = True
    process_log_file(args.filename, args)

if __name__ == "__main__":
    main()
