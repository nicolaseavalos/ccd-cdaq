#!/usr/bin/env python3

import libABCD
import argparse, logging

### parse command line args
parser = argparse.ArgumentParser(description='Pause/unpause a queue.')
parser.add_argument('queue', nargs='*', default=['default'], help='the queue name(s)')
parser.add_argument('-u', '--unpause', action='store_true')
parser.add_argument('-m', '--message', default='')
args = parser.parse_args()

libABCD.init('pausequeue', expconfig='experiment/default.ini')

for queue in args.queue:

    if args.unpause:
        cmd = 'unpause()'

    else:
        cmd = f'pause("{args.message}")'
    
    print(f'> Sending "{cmd}" to "run/{queue}/inputs"')
    libABCD.publish(f'run/{queue}/inputs', payload=f'{cmd}')

libABCD.disconnect()
