#!/usr/bin/env python3

import libABCD    
import argparse, logging

parser = argparse.ArgumentParser(description='Add a command to a run_control queue.')
parser.add_argument('message', default=None, type=str,
                    help='the command to be sent')
parser.add_argument('--to', type=str, required=True, 
                    help='the target of the message')
parser.add_argument('--qname', default='default', type=str, 
                    help='the queue to which the command will be appended')
parser.add_argument('--qtype', default='queue', type=str, choices=['queue', 'repeat'],
                    help='the queue type')
args = parser.parse_args()

libABCD.init('sendrun', expconfig='experiment/default.ini', loglevel=logging.WARNING)
from source._utilities import add2runqueue
add2runqueue(args.message, args.to, args.qname, args.qtype, verbose=True)
libABCD.disconnect()
