#!/usr/bin/env python3

import libABCD
import argparse, logging


parser = argparse.ArgumentParser(description='Send messages to a CCD client.')
parser.add_argument('ccdid', help='the name of the target CCD client')
parser.add_argument('cmd', nargs='+', help='the command to be sent')
args = parser.parse_args()

libABCD.init('sendccd', expconfig='experiment/default.ini', loglevel=logging.WARNING)
from source._utilities import send2ccd, _send_generic

if args.cmd[0] == 'stop':
    timeout = ''
    if len(args.cmd)>1:
        timeout = args.cmd[1]
    _send_generic(f'ccd_clients/{args.ccdid}/stop', timeout, verbose=True)

else:
    send2ccd(args.cmd[0], args.ccdid, verbose=True)

libABCD.disconnect()
