#!/usr/bin/env python3

import argparse
import libABCD

# parse command line arguments
parser = argparse.ArgumentParser(description='Lift an emergency stop for a CCD client.')
parser.add_argument('ccdid', help='the CCD client')
parser.add_argument('--expconfig', default='experiment/default.ini',
                    help='the experiment configuration file')
args = parser.parse_args()

user_input = input('>>> Are you sure you want to lift the emergency stop? (Yes/[no])')
if user_input not in ['yes', 'y', 'Y']:
    print('Aborting.')
    quit()


libABCD.init('lift_emergency_stop', expconfig=args.expconfig)
from source._utilities import send2ccd

send2ccd('lift_emergency_stop', args.ccdid)

libABCD.disconnect()