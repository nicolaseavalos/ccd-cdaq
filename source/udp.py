#!/usr/bin/env python3

import libABCD
import time, logging, os
from datetime import date, datetime

### parse command line args
import argparse
parser = argparse.ArgumentParser(
                    description='Listen to all MQTT topics.')
parser.add_argument('--expconfig', type=str,
                    default='experiment.ini',
                    help='a configuration file for the experiment')
args = parser.parse_args()

expconfig = args.expconfig

### main configuration
name = "udp"
logger = logging.getLogger(name)


### initialize mqtt and logger
libABCD.init(name, expconfig=expconfig, fileloglevel=logging.DEBUG, unique=True)
logger.info('udp ready to process raw messages')

import socket

def udp_listener(host='0.0.0.0', port=12345):
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the address and port
    sock.bind((host, port))

    while True:
        # Receive data from the socket
        data, addr = sock.recvfrom(1024)  # Buffer size is 1024 bytes
        logger.info(f"Got: 0x{data.hex()} from {addr}")
        logger.warning(f"{data.hex()}")


### loop
try:
    udp_listener(port=8226)  # Change the port number as needed

except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

finally:
    libABCD.disconnect()
