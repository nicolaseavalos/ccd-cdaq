#!/usr/bin/env python3

### imports
import libABCD
import argparse, json, logging, os, signal, threading, time


### parse command line args
parser = argparse.ArgumentParser(description='Send commands to a ccd.')
parser.add_argument('--ccdid', type=str, required=True, 
                    help='the ccd identification')
parser.add_argument('--daq', choices=['fake', 'lta', 'pylta', 'ACM', 'leach', 'Monit'], 
                    required=True, help='the daq type')
parser.add_argument('--daq_kwds', type=str, default='{}',
                    help='specific DAQ object keywords')
parser.add_argument('--monitoring', type=str,  
                    help='the associated monitoring or safety watchdog')                   
parser.add_argument('--expconfig', type=str, default='experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
pargs = parser.parse_args()


### client configuration
expconfig = pargs.expconfig
debug = pargs.debug
ccdid = pargs.ccdid
daqname = pargs.daq
daq_kwds = json.loads(pargs.daq_kwds)
if debug:
    loglevel = logging.DEBUG
else: 
    loglevel = logging.INFO


### global variables
ccdpool = None
client_name = 'ccd_client.'+ccdid
bShutdown = False


### logger
logger = logging.getLogger(client_name)


### initialize libABCD
libABCD.init(client_name, expconfig=expconfig, loglevel=loglevel,
             fileloglevel=logging.DEBUG, listener=True,
             report_status=True, pingpong=True, unique=True)

from _utilities import mqttcleanretained


### initialize DAQ object
from libDAQ import parse_daq
daq = parse_daq(daqname, ccdid, **daq_kwds)
libABCD.publish(f'ccd_clients/{ccdid}/daq', daqname, retain=True)


### execution thread
class ExecutionThread:

    def __init__(self) -> None:

        self._t = threading.Thread()
        self._t.start()

        self.current = None
        self.status = 'standby'


    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, value):

        self._current = value
        self._ctime = time.time()
        libABCD.publish(f'ccd_clients/{ccdid}/current', 
                        value, retain=True)
        libABCD.publish(f'ccd_clients/{ccdid}/ctime', 
                        self._ctime, retain=True)


    def _exec_cmd(self, cmd):

        logger.info(f'Executing: {cmd}')
        _status('ExecutingCmd')

        try:
            exec(cmd)
        except Exception as e:
            logger.exception(f'Could not execute following command: {cmd}. Error: {e}.')
        


    def exec(self, cmd: str):
               
        self.current = cmd
        # daemon=True is needed to kill the client via mqtt
        self._t = threading.Thread(target=self._exec_cmd, args=(cmd,), daemon=True)  
        self._t.start()


    def join(self):

        if not self._t.is_alive():
            if client_status == 'ExecutingCmd':
               _status('WaitingForMsg')
               self.current = None
        time.sleep(.1)
            
    
    def stop(self, timeout):
        '''LDAQ must have a `stop_current` method that interrupts the
        execution'''
        
        old_current = self.current

        cmd = 'daq.stop_current()'
        self.current = cmd

        self._exec_cmd(cmd)

        self._t.join(timeout=timeout)

        if self._t.is_alive():
            logger.warning(f'"stop" command timed out. Client is still doing work.')
            self._t = threading.Thread()
            self._t.start()
        

### MQTT callbacks
def ccd_callback(msg: dict, topic: str):

    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    prev_client_status = client_status
    _status('MsgReceived')

    # parse payload
    pmsg = msg['payload']

    if type(pmsg) == str:
        
        if et._t.is_alive():
            logger.error(f'Client is busy. Ignoring "{pmsg}" call.')
            _status(prev_client_status)
            return
        
        et.exec(pmsg)

    else:
        logger.warning(f'Ignoring payload: "{pmsg}". Wrong type.')
        _status(prev_client_status)
        

def shutdown_callback(msg: dict, topic: str):

    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    logger.info('Shutting down upon user request (via mqtt).')
    global bShutdown
    bShutdown = True
    et.stop(timeout=1)
    os.kill(os.getpid(), signal.SIGINT)


def stop_callback(msg: dict, topic: str):
    
    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    pmsg = msg['payload']    

    if type(pmsg)in [int, float]:
        timeout = pmsg
    else: timeout = 10
    
    et.stop(timeout)


### client status
def _status(s):

    global client_status
    
    client_status = s
    libABCD.publish(f'ccd_clients/{ccdid}/status', client_status, retain=True)


### pool
def listen2pool(poolname):

    global ccdpool

    if poolname:
        if type(poolname) != str:
            logger.error('"poolname" is not a string.')
            return

    elif poolname is not None:
        logger.warning('Received empty pool name. Nothing will happen.')
        return

    if poolname != ccdpool:

        # stop listening to old pool
        if ccdpool:
            logger.info(f'Will stop listening to topic "ccd_pools/{ccdpool}/inputs"')
            libABCD.unsubscribe(f'ccd_pools/{ccdpool}/inputs')

        if poolname == 'None': poolname = None

        # publish the pool
        libABCD.publish(f'ccd_clients/{ccdid}/pool', poolname, retain=True)

        # start listening to new pool
        ccdpool = poolname

        if poolname is None:
            libABCD.add_callback(f'ccd_clients/{ccdid}/inputs', ccd_callback)
            return

        logger.info(f'Will start listening to topic "ccd_pools/{ccdpool}/inputs"')
        libABCD.add_callback(f'ccd_pools/{ccdpool}/inputs', ccd_callback)


### initialize
et = ExecutionThread()
listen2pool(None)
_status('WaitingForMsg')
logger.info('DAQ initialized. Waiting for command.')

libABCD.add_callback(f'ccd_clients/{ccdid}/inputs', ccd_callback)
libABCD.add_callback(f'ccd_clients/{ccdid}/shutdown', shutdown_callback)
libABCD.add_callback(f'ccd_clients/{ccdid}/stop', stop_callback)


### MAIN LOOP
try:
    while not bShutdown:
        et.join()
        
except KeyboardInterrupt:
    if not bShutdown:
        logger.info('Shutting down upon user request (via ctrl-c).')

except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.', exc_info=True)

finally:
    _status('Offline')
    for topic in ['pool', 'daq', 'current', 'status', 'ctime']:
        mqttcleanretained(f'ccd_clients/{ccdid}/'+topic)
    libABCD.disconnect()
