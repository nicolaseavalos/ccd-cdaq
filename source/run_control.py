#!/usr/bin/env python3

from typing import Any
import libABCD
import argparse, logging, time


### parse command line arguments
parser = argparse.ArgumentParser(
                description='Manage an experiment run via queues.')
parser.add_argument('--expconfig', type=str, 
                    default='experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
args = parser.parse_args()
expconfig = args.expconfig
debug = args.debug

if debug:
    loglevel = logging.DEBUG
else: 
    loglevel = logging.INFO

### global variables
name = 'run_control'
logger = logging.getLogger(name)

### init libabcd
libABCD.init(name, expconfig=expconfig, loglevel=loglevel,
             fileloglevel=logging.DEBUG, listener=True,
             report_status=True, pingpong=True, unique=True)

from daq_queue import RunDAQQueue
from _utilities import send2queuemanager, send2ccd


### useful classes
class CCDClient():

    active = {}

    def __new__(cls, cname):

        if cname in CCDClient.active:
            return CCDClient.active[cname]
        
        self = object.__new__(cls)

        self.name = cname
        self.status = None
        self.current = None
        self.queue = None
        self.incomingMsg = False

        self.add2queue('default')
        CCDClient.active[cname] = self

        libABCD.add_callback(f'ccd_clients/{cname}/+', ccd_client_callback)
        return self


    def __setattr__(self, __name: str, __value: Any) -> None:

        object.__setattr__(self, 'last_update', time.time())
        object.__setattr__(self, __name, __value)


    def add2queue(self, qname):

        if qname in ['None', 'none']: qname = None

        old_queue = self.queue
        if qname != old_queue:
            
            if old_queue:
                oq = RunDAQQueue(old_queue)
                oq.remove_target(self.name)

            if qname:
                nq = RunDAQQueue(qname)
                nq.add_target(self.name)


    def delete(self):
        libABCD.unsubscribe(f'ccd_clients/{self.name}/+')

        if self.queue:
            q = RunDAQQueue(self.queue)
            q.remove_target(self.name)
        del CCDClient.active[self.name]


### create default queue
dqueue = RunDAQQueue('default')
dqueue.add_target('qmanager')


### libABCD callbacks
def ccd_client_callback(msg: dict, topic: str):

    key = topic.rpartition('/')[2]
    if key not in ['status', 'current']: return

    ccdid = topic.partition('/')[2].rpartition('/')[0]

    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    value = msg['payload']

    c = CCDClient(ccdid)

    if c.status == 'WaitingForMsg' and c.incomingMsg and value != 'WaitingForMsg':
        c.incomingMsg = False

    c.__setattr__(key, value)


def ccdmanager_callback(msg: dict, topic: str):

    active_list = msg['payload']

    # create new CCDClients
    for aname in active_list:
        c = CCDClient(aname)

    # delete inactive CCDClients
    for cname in CCDClient.active.copy():
        if cname not in msg['payload']:
            CCDClient.active[cname].delete()


def queue_callback(msg: dict, topic: str):

    logger.debug(f'Received message "{msg}" on topic "{topic}"')
    # reply_msg(msg)

    queuename = topic.partition('/')[2].rpartition('/')[0]
    if queuename not in RunDAQQueue.active:
        return

    cmd = msg['payload']
    fullcmd = f'{queuename}.{cmd}'

    local_queues_dict = RunDAQQueue.active.copy()
    try:
        logger.info(f'Executing command "{cmd}" on queue "{queuename}"')
        exec(fullcmd, local_queues_dict)

    except Exception as e:
        logger.error(f'Could not execute following command: {cmd}. Error: {e}.')
        return
    

libABCD.add_callback('ccdmanager/active', ccdmanager_callback)
libABCD.add_callback('run/+/inputs', queue_callback)

try:
    while True:
        
        start_time = time.monotonic()

        for queuename, q in RunDAQQueue.active.copy().items():

            if q.current is None:
                next_cmd = q.return_next()

                if next_cmd:
                    tocmd = next_cmd['to']
                    cmd = next_cmd['cmd']

                    if tocmd == 'qmanager':
                        send2queuemanager(cmd)
                        continue

                    if tocmd not in CCDClient.active.copy():
                        logger.warning(f'{tocmd} is not an active CCD client! Pausing {queuename} until this is solved.')
                        q.reset_current()
                        q.pause(f'{tocmd} is offline')
                        continue

                    c = CCDClient(tocmd)

                    # first check if client is available for receiving commands
                    if c.status != 'WaitingForMsg':
                        logger.warning(f'{tocmd} is not ready for commands yet.')
                        q.reset_current({'to': tocmd, 'cmd': c.current})
                        
                        continue
                    
                    c.incomingMsg = True
                    send2ccd(cmd, tocmd)
                    while c.incomingMsg:
                        time.sleep(.001)

            
            else:
                target = q.current[0]['to']

                if target == 'qmanager': 
                    q.set_current(None)
                    continue

                if target in CCDClient.active:
                    c = CCDClient(target)
                    if c.status == 'WaitingForMsg':
                        q.set_current(None)

                else:
                    logger.warning(f'{target} went inactive while awaiting for completion. Pausing {queuename} until this is solved.')
                    q.set_current(None)
                    q.pause(f'{tocmd} is offline')
                    
        elapsed = time.monotonic() - start_time
        if elapsed < 0.1:
            time.sleep(.1-elapsed)

except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

finally:
    for qname, q in RunDAQQueue.active.copy().items():
        q.delete()
    libABCD.disconnect()