import libABCD
import json, logging, os, shlex, subprocess


logger = logging.getLogger(libABCD.name)


def return_screen(name: str, user=None, host=None):
    """Check if a screen with that name is running"""

    # define the command to send
    sargs = 'ls /var/run/screen/S-'
    if not user:
        user = os.getenv('USER')
    else:
        if not host: 
            logger.error('"host" must be specified if "user" is not None')
        sargs = f'ssh {user}@{host} ' + sargs
    sargs = sargs + user

    # get a list of the screens
    lsargs = shlex.split(sargs)
    lsp = subprocess.run(lsargs, stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE, universal_newlines=True)
    lsout = lsp.stdout.split()

    for sname in lsout:
        if name in sname:
            return sname
    
    return None


def parse_ccdmanager_string(s: str or list): # type: ignore

    if type(s) == str:
        arg_list = s.split()
    elif type(s) == list:
        arg_list = s
    else:
        raise TypeError

    cmd = arg_list[0]
    args = []
    kwargs = {}

    if len(arg_list) > 1:
        args = arg_list[1:]

    for arg in args:
        if '=' in arg:
            key, foo, value = arg.partition('=')
            kwargs[key] = value 

    args = [arg for arg in args if '=' not in arg]

    return cmd, args, kwargs


# reply by echoing the message
def reply_msg(msg):
    libABCD.publish(f'{msg["from"]}/answers', msg['payload'])


def _send_generic(topic, cmd, verbose=False):

    if verbose:
        print(f'> Publishing "{cmd}" to "{topic}"')

    libABCD.publish(topic, cmd)

    return


def send2ccd(cmd, ccdid, verbose=False):

    return _send_generic(f'ccd_clients/{ccdid}/inputs', cmd, verbose=verbose)
        

def send2pool(cmd, poolname, client_list=[], verbose=False):

    topic_list = [ f'ccd_client.{clientname}' for clientname in client_list ]
    return _send_generic(f'ccd_pools/{poolname}/inputs', cmd, verbose=verbose)


def add2runqueue(msg, to, queuename='default', queuetype='queue', verbose=False):

    dmsg = {"to": to, "cmd": msg}
    topic = f'run/{queuename}/inputs'

    if queuetype == 'queue':
        cmd = f'add_queue({dmsg})'
    
    elif queuetype == 'repeat':
        cmd = f'add_repeat({dmsg})'

    else:
        logger.error(f'Invalid queue type: {queuetype}. Nothing will happen.')
        return

    return _send_generic(topic, cmd, verbose=verbose)


def send2queuemanager(cmd, qname='default', verbose=False):

    return _send_generic(f'run/{qname}/inputs', cmd, verbose=verbose)


def mqttcleanretained(topic):
    libABCD.mqttp.publish(topic, None, retain=True)
    logger.debug(f'Cleaning retained messages on topic {topic}')


# class JSONHandler(PatternMatchingEventHandler):
#     def __init__(self, patterns=None, ignore_patterns=None, ignore_directories=False, case_sensitive=False):
#         super().__init__(patterns, ignore_patterns, ignore_directories, case_sensitive)

#     def on_created(self, event):
#         return super().on_created(event)

#     def on_modified(self, event):
#         return super().on_modified(event)

#     def on_deleted(self, event):
#         logger.critical(f'File deleted while being watched: {event.src_path}')
#         raise FileNotFoundError(f'{event.src_path}')


# def watch_json(jsonfile, modified_callback):

#     json_path = 'json/'

#     if not os.path.exists(os.path.join(json_path,jsonfile)):
#         raise FileNotFoundError(f'{jsonfile} does not exist')

#     event_handler = JSONHandler([jsonfile])
#     event_handler.on_modified = modified_callback

#     observer = Observer()
#     observer.schedule(event_handler, path=json_path)
#     observer.start()

#     return observer

# dump to JSON
def dump2json(d, jsonfile):
    
    os.makedirs('json', exist_ok=True)
    with open(f'json/{jsonfile}', 'w+') as fp:
        json.dump(d, fp, indent=2)