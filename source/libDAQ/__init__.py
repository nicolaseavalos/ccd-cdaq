def parse_daq(daqname, *args, **kwds):
    if daqname == 'lta':
        from liblta import LTA
        return LTA(*args, **kwds)
    if daqname == 'pylta':
        from liblta import pyLTA
        return pyLTA(*args, **kwds)
    if daqname == 'leach':
        from .libLeach import Leach
        return Leach()
    if daqname == 'fake':
        from .libFake import FakeDAQ
        return FakeDAQ(*args, **kwds)
    if daqname == 'ACM':
        from .libACM import ACM
        return ACM(*args)
    if daqname == 'Monit':
        from .libMonit import Monit
        return Monit(*args, **kwds)

    else:
        raise ValueError(f'Error: unknown DAQ ({daqname}).')
    
# XXX IMPLEMENT
def get_decoder(daqname, *args, **kwds):
    if daqname == 'lta':
        return
    if daqname == 'leach':
        return
    if daqname == 'fake':
        from .libFake import FakeDecoder
        return FakeDecoder(*args, **kwds)
    if daqname == 'acm':
        from .libACM import DecoderAndExporter
        return DecoderAndExporter(ch_list=['CCD_A', 'CCD_B', 'CCD_D', 'CCD_C'])
    else:
        raise ValueError(f'Error: unknown DAQ ({daqname}).')
    
