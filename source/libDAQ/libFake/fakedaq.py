import logging, time, sys
from datetime import datetime
from typing import Any

# logic to communicate with CDAQ
msgPub = False
try:
    import libABCD
    if libABCD.mqttp:
        msgPub = True
except:
    pass

# Initialize logger
logger = logging.getLogger('fakedaq')

# global logging level
logger.setLevel(logging.DEBUG)

# if a root logger was defined elsewhere, do not add handlers
if logger.hasHandlers():
    logger.addHandler(logging.NullHandler())

else:
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)-25s - %(levelname)-8s - %(message)s')

    # create console handler
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    logger.addHandler(ch)


class FakeDAQ:

    def __init__(self, name, **kwargs):

        self.stop = False
        self.running = False
        self.imid = 0
        self.name = name
        self.logger = logging.getLogger('fakedaq.'+name)

        for k,v in kwargs.items():
            self.logger.info(f'Got keyword "{k}" with value "{v}"')


    def doOneImage(self, name=None, **kwargs):

        self.running = True
        self.logger.info('Pretending to be taking image...')

        if 'wait' in kwargs:
            wait = kwargs['wait']
        else: wait = 5

        start_time = time.monotonic()
        self.stop = False

        while time.monotonic() - start_time < wait:
            time.sleep(.5)
            if self.stop:
                self.logger.info('Fake image aborted')
                self.running = False
                self.stop = False
                return

        if name:
            name = f'images/{name}_{self.name}_{self.imid}.bin'
            self.imid += 1
            # create a fake file
            with open(name, 'wb') as f:
                now = datetime.now().isoformat(timespec='seconds')
                s = f'file created at {now}'
                enc_s = bin(int.from_bytes(s.encode(), 'big'))
                f.write(enc_s.encode('utf8'))

        self.logger.info('Fake image finished.')
        if msgPub:
            libABCD.publish('ccdoutput/fake', {'imgname': name})
        self.running = False


    def stop_current(self):

        if not self.running:
            self.logger.info('Nothing to stop.')
        else:
            self.logger.info('Trying to abort...')
            self.stop = True
            while self.running:
                time.sleep(1)
            

    def emergency_stop(self):

        self.logger.info('Activating emergency stop.')
        self.stop_current()

    
    # def lift_emergency_stop(self):

    #     if self.emergency:
    #         logger.extra('Deactivating emergency stop.')
    #         self.emergency = False

    #     else:
    #         logger.info('No emergency stop to deactivate.')


class FakeDecoder:
    
    def __init__(self):
        self.logger = logging.getLogger('fakedaq.decoder')
        self.logger.info('Fake decoder initalized.')

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        self.logger.info('Fake decoder called')
        if 'imgname' in kwds and kwds['imgname'] is not None:
            self.logger.info(f'Processing {kwds["imgname"]}')
            with open(kwds['imgname'], 'r') as f:
                enc_s = f.read()
            n = int(enc_s, 2)
            dec_s = n.to_bytes((n.bit_length() + 7) // 8, 'big').decode()
            with open(kwds['imgname'].rpartition('.')[0]+'.txt', 'w') as f:
                f.write(dec_s)
        if 'time' in kwds:
            time.sleep(kwds['time'])
        else:
            time.sleep(5)
        self.logger.info('Fake decoder finished')
