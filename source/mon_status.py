#!/usr/bin/env python3

''' mon_status.py
This app monitors everything that goes online in libABCD with 
`report_status=True`, and dumps the dictionary with the date to a JSON 
file. It also sends a 'ping' signal and listens to the answers, then
cross-checks 'pongs' with 'pings'.
'''

import libABCD
import argparse, logging, os, time
from datetime import datetime


### global variables
name = 'mon_status'
status_dict = {}    # dictionary with entries like 'clientname': {'status': status, 'pongtime': pongtime}


### parse command line arguments
parser = argparse.ArgumentParser(
                    description='Ping and monitor CDAQ apps.')
parser.add_argument('--expconfig', type=str, 
                    default='experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
args = parser.parse_args()
debug = args.debug
if debug:
    loglevel = logging.DEBUG
else: loglevel = logging.INFO

expconfig = args.expconfig

logger = logging.getLogger(name)

### initialize libABCD
libABCD.init(name, expconfig=expconfig, loglevel=loglevel, 
             fileloglevel=logging.DEBUG, listener=True, 
             report_status=True, unique=True)

from _utilities import dump2json, mqttcleanretained


### libABCD callbacks
# if a libABCD client has 'report_status=True', it publishes to status/{clientname}
def status_callback(msg: dict, topic: str):

    # parse the information
    try:
        clientname = topic.partition('status/')[2]
        statusfrom = msg['from']
        statustime = msg['timestamp']
        status = msg['payload']

    except:
        return   # silent if fail

    # do not track 'listeners'
    if 'listener' in clientname: return

    # do not track myself
    if clientname == name: return

    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    # compare the new info with the old one
    if clientname in status_dict:
        oldstatus = status_dict[clientname]['status']
        if status == oldstatus: return
        logger.info(f'Received new status from {clientname}: {status} (was {oldstatus})')
        status_dict[clientname]['pongtime'] = time.time()

    else:
        status_dict[clientname] = {}
        # log only when first 'on' arrives
        if status == 'on':
            logger.info(f'Now tracking {clientname}')
            status_dict[clientname]['pongtime'] = time.time()

    modify_status(clientname, 'status', status)


# if a libABCD client has 'pingpong=True', it answers to 'pings' on topic 'pongs'
def pong_callback(msg: dict, topic: str):

    # parse the information
    try:
        pongfrom = msg['from']

    except:
        return   # silent if fail
    
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    pongtime = time.time()
    if pongfrom in status_dict:
        print(f'{datetime.fromtimestamp(pongtime).strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]} - {pongfrom: <15} - pong')
        modify_status(pongfrom, 'pongtime', pongtime)

        if status_dict[pongfrom]['status'] != 'on':
            logger.info(f'{pongfrom} is back online!')
            modify_status(pongfrom, 'status', 'on')


libABCD.add_callback('status/+', status_callback)
libABCD.add_callback('pongs', pong_callback)


### mon_status specific functions
def publish_status():
    mqtt_dict = { k:status_dict[k]['status'] for k in status_dict }
    libABCD.publish('mon_status', mqtt_dict, retain=True)


def modify_status(clientname, key, val):
    status_dict[clientname][key] = val
    dump2json(status_dict, 'mon_status.json')
    if key == 'status': publish_status()


def check_pongs():

    for clientname in status_dict:

        if status_dict[clientname]['status'] != 'on':
            continue

        now = time.time()
        if 'pongtime' not in status_dict[clientname]: return
            
        elapsed = now - status_dict[clientname]['pongtime']
        if elapsed < 30:
            continue
        logger.warning(f'{clientname} is unresponsive (has not answered "pong" in the last 30 seconds)')

        modify_status(clientname, 'status', 'unresponsive')


### loop
try:
    while True:

        # publish ping
        print(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]} - {name: <15} - ping')
        libABCD.publish('pings')
        
        time.sleep(10)

        # check answers
        print(f'status_dict: {status_dict}')
        check_pongs()

    
except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

finally: 
    mqttcleanretained('mon_status')
    libABCD.disconnect()
    os.remove('json/mon_status.json')
    