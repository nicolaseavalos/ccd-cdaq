#!/usr/bin/env python3

import libABCD
import time, logging, os
from datetime import date, datetime

### parse command line args
import argparse
parser = argparse.ArgumentParser(
                    description='Listen to all MQTT topics.')
parser.add_argument('--expconfig', type=str, 
                    default='experiment.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
parser.add_argument('--ignore-pings', action='store_true',
                    help='do not include ping-pong in log')
args = parser.parse_args()
debug = args.debug
if debug:
    loglevel = logging.DEBUG
else: loglevel = logging.INFO

expconfig = args.expconfig

### main configuration
name = "Spy"
logger = logging.getLogger(name)


### initialize mqtt and logger
libABCD.init(name, expconfig=expconfig, loglevel=loglevel,
             fileloglevel=logging.DEBUG, listener=True, 
             report_status=True, pingpong=True, unique=True)


### mqtt callbacks
def on_message(msg, topic):

    if args.ignore_pings and topic in ['pings', 'pongs']:
        return

    save_msg(time.time(), topic, msg['from'], msg['payload'])

    print(f'{topic: <25} - {msg}')
    

libABCD.add_callback('#', on_message)


def save_msg(msgtime, msgtopic, msgfrom, msg):

    filename = f'log/Spy_{date.today().isoformat()}.csv'
    msgdatetime = datetime.fromtimestamp(msgtime).isoformat(timespec='milliseconds')

    new_day = False

    if not os.path.exists(filename):
        new_day = True

    with open(filename, 'a+') as f:
        if new_day:
            f.write('datetime,topic,from,msg\n')

        s = f'{msgdatetime},{msgtopic},{msgfrom},{msg}\n'

        f.write(s)


### loop
try:
    while True:
        time.sleep(1)
    
except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

finally: 
    libABCD.disconnect()