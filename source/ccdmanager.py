#!/usr/bin/env python3

### imports
import libABCD
import argparse, logging, os, time


### parse command line args
parser = argparse.ArgumentParser(description='Manage CCD clients.')
parser.add_argument('--expconfig', type=str, 
                    default='experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
args = parser.parse_args()


### client configuration
expconfig = args.expconfig
if args.debug:
    loglevel = logging.DEBUG
else: 
    loglevel = logging.INFO


### global variables
name = 'ccdmanager'
logger = logging.getLogger(name)


### initialize libABCD
libABCD.init(name, expconfig=expconfig, loglevel=loglevel,
             fileloglevel=logging.DEBUG, listener=True,
             report_status=True, pingpong=True, unique=True)

from _utilities import dump2json, mqttcleanretained, send2pool


def _dump_dicts():
    big_dict = {**{cname:c.__dict__ for cname,c in CCDClient.active.items()},
                **{pname:p.__dict__ for pname,p in CCDPool.active.items()}}
    dump2json(big_dict, 'ccdmanager.json')


def _publish_active():

    clist = [ cname for cname in CCDClient.active.copy() ]
    plist = [ pname for pname in CCDPool.active.copy() ]

    libABCD.publish('ccdmanager/active', clist+plist, retain=True)


class CCDBase():

    def __setattr__(self, __name: str, __value) -> None:

        try:
            logger.debug(f'New "{__name}" for "{self.name}": "{__value}".')
        except AttributeError:
            pass
        object.__setattr__(self, 'last_update', time.time())
        object.__setattr__(self, __name, __value)
        _dump_dicts()


class CCDClient(CCDBase):

    active = {}

    def __new__(cls, clientname: str):

        if clientname in CCDClient.active:
            return CCDClient.active[clientname]

        self = object.__new__(cls)

        self.name = clientname
        self.daq = None
        self.pool = None
        self.status = None
        self.current = None
        self.mqttstatus = None
        self.IncomingMsg = False

        self.active[clientname] = self

        logger.info(f'Created CCDClient "{clientname}"')

        for key in ['daq', 'pool', 'status', 'current']:
            libABCD.add_callback(f'ccd_clients/{clientname}/{key}', ccd_client_callback)

        _publish_active()
        # libABCD.publish(f'ccd_clients/{self.name}/update')

        return self


    def set_status(self, new_status):

        self.status = new_status

        if self.IncomingMsg and new_status == 'MsgReceived':
            self.IncomingMsg = False


    def set_pool(self, poolname):

        prev_pool_name = self.pool
        new_pool_name = None if poolname in ['None', 'none'] else poolname

        new_pool = CCDPool(new_pool_name) if new_pool_name else None

        if prev_pool_name:
            prev_pool = CCDPool(prev_pool_name)
            if prev_pool != new_pool:
                prev_pool.remove_client(self.name)

        if new_pool: new_pool.add_client(self.name)
        self.pool = new_pool_name


    def delete(self):
        if self.pool:
            logger.warning(f'CCD client {self.name} went offline while being part of pool {self.pool}')
        self.set_pool(None)
        for key in ['daq', 'pool', 'status', 'current']:
            libABCD.unsubscribe(f'ccd_clients/{self.name}/{key}')
        del self.active[self.name]
        _publish_active()
        _dump_dicts()


class CCDPool(CCDBase):

    active = {}

    def __new__(cls, poolname):
        
        if poolname in CCDPool.active:
            return CCDPool.active[poolname]

        self = object.__new__(cls)

        self.name = poolname
        self.clients = []
        self.status = None
        self.current = None
        self.next_cmd = None

        logger.info(f'Created CCDPool "{poolname}"')
        libABCD.add_callback(f'ccd_clients/{poolname}/inputs', ccd_pool_callback)

        self.active[poolname] = self

        self.set_status('WaitingForMsg')

        _publish_active()
        return self


    def __setattr__(self, __name: str, __value) -> None:

        super().__setattr__(__name, __value)
        if __name == 'name': return
        libABCD.publish(f'ccd_clients/{self.name}/{__name}', __value, retain=True)


    def set_status(self, status: str):

        if status == 'MsgReceived':
            for c in self.clients:
                c = CCDClient(c)
                c.IncomingMsg = True
        self.status = status
        # libABCD.publish(f'ccd_clients/{self.name}/status', status, retain=True)


    def set_current(self, current: str):

        self.current = current
        # libABCD.publish(f'ccd_clients/{self.name}/current', current, retain=True)


    def add_client(self, clientname):

        if clientname not in CCDClient.active.copy():
            logger.error(f'Error adding {clientname} to {self.name}: client does not appear active')
            return
        
        if clientname not in self.clients:
            self.clients = self.clients + [clientname]
            logger.info(f'Added {clientname} to pool {self.name}')


    def remove_client(self, clientname):

        try:
            clients = self.clients.copy()
            clients.remove(clientname)

        except ValueError:
            logger.error(f'Error removing {clientname} from {self.name}: client is not in this pool.')
            return

        self.clients = clients
        logger.info(f'Removed {clientname} from pool {self.name}')

        if len(self.clients) == 0:
            logger.info(f'No clients remain for {self.name}')
            self.delete()


    def set_next_cmd(self, new_cmd):

        self.next_cmd = new_cmd


    def check_status(self):

        # check if message arrived to all clients,
        if any([CCDClient(cname).IncomingMsg for cname in self.clients]):
            return
        
        # then if all clients are finished
        if self.status == 'ExecutingCmd' and\
                all([CCDClient(cname).status == 
                     'WaitingForMsg' for cname in self.clients]):
            self.set_status('WaitingForMsg')
            self.current = None


    def delete(self):

        libABCD.unsubscribe(f'ccd_clients/{self.name}/inputs')
        for key in ['status', 'clients', 'current', 'next_cmd']:
            mqttcleanretained(f'ccd_clients/{self.name}/{key}')
        logger.info(f'Deleted pool {self.name}')
        del self.active[self.name]
        _publish_active()
        _dump_dicts()


_dump_dicts()


### libabcd callbacks
def ccd_client_callback(msg: dict, topic: str):

    key = topic.rpartition('/')[2]
    if key not in ['daq', 'pool', 'status', 'current']: return

    ccdid = topic.partition('/')[2].rpartition('/')[0]
    if ccdid in CCDPool.active: return

    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    value = msg['payload']

    c = CCDClient(ccdid)

    if getattr(c, key) != value:
        if key == 'status':
            c.set_status(value)
        elif key == 'pool':
            c.set_pool(value)
        else: c.__setattr__(key, value)


def ccd_pool_callback(msg: dict, topic: str):
    
    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    # parse
    poolname = topic.partition('/')[2].rpartition('/')[0]
    p = CCDPool(poolname)

    prev_pool_status = p.status
    p.set_status('ProcessingMsg')

    # parse payload
    pmsg = msg['payload']

    if type(pmsg) == str:
        new_cmd = msg['payload']

    else:
        logger.warning(f'Ignoring payload: "{pmsg}". Wrong type.')
        p.set_status('MsgRejected')
        p.set_status(prev_pool_status)
        return

    if new_cmd == 'stop':
        logger.info('Received a "stop" signal.')
        p.set_status('MsgReceived')

    else:
        if prev_pool_status != 'WaitingForMsg':
            logger.error('Received a command while the pool is busy. Ignoring.')
            p.set_status('MsgRejected')
            p.set_status(prev_pool_status)
            return

        else:
            p.set_status('MsgReceived')
    
    p.set_next_cmd(new_cmd)


def status_callback(msg: dict, topic: str):

    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    pmsg = msg['payload']   # dictionary with entries like 'clientname': 'status'

    for key, val in pmsg.items():
        if 'ccd_client.' not in key: continue
    
        ccdid = key.partition('ccd_client.')[2]

        if ccdid not in CCDClient.active: 
            if val == 'off': continue

        c = CCDClient(ccdid)

        prev_mqtt_status = c.mqttstatus

        if val != prev_mqtt_status:
            logger.info(f'Received new MQTT status for {ccdid}: {val} (was {prev_mqtt_status})')
            c.mqttstatus = val
            if val == 'off': 
                c.delete()


# add the callbacks
libABCD.add_callback('mon_status', status_callback)


try:
    while True:

        # for ccdid in ccd_clients_dict:
        #     if ccd_clients_dict[ccdid]['daq'] is None:
        #         if time.time() - ccd_clients_dict[ccdid]['last_update'] > 10:
        #             logger.info(f'Requesting missing information from {ccdid}')
        #             libABCD.publish(f'ccd_clients/{ccdid}/update')
        #             ccd_clients_dict[ccdid]['last_update'] = time.time()

        for poolname in CCDPool.active.copy():
            p = CCDPool(poolname)
            p.check_status()
            if p.next_cmd:
                p.set_status('ExecutingCmd')
                p.set_current(p.next_cmd)
                send2pool(p.next_cmd, poolname)
                p.next_cmd = None

        time.sleep(.1)
        
except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

finally:
    for pname, p in CCDPool.active.items():
        p.delete()
    libABCD.disconnect()
    os.remove('json/ccdmanager.json')
