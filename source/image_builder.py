#!/usr/bin/env python3

### imports
import libABCD
import argparse, json, logging, threading, time
from libDAQ import get_decoder


### parse command line args
parser = argparse.ArgumentParser(description='Send commands to a ccd.')
parser.add_argument('--imbid', type=str,
                required=True, help='the builder identification')
parser.add_argument('--daq', choices=['fake', 'lta', 'acm', 'leach'], 
                    required=True, help='the daq type')
parser.add_argument('--dec_kwds', type=str, default='{}',
                    help='keyword arguments for decoder object')
parser.add_argument('--expconfig', type=str, 
                    default='../experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('--debug', action='store_true', 
                    help='log debug messages')
args = parser.parse_args()


### global variables
# parsed
expconfig = args.expconfig
debug = args.debug
imbid = args.imbid
daqname = args.daq
dec_kwds = json.loads(args.dec_kwds)
if debug:
    loglevel = logging.DEBUG
else: 
    loglevel = logging.INFO

# inferred
client_name = 'image_builder.'+imbid
logger = logging.getLogger(client_name)

# not initialized
imbuild_status = 'Starting'
t = threading.Thread()
bShutdown = False
bPause = False


### initialize libABCD
libABCD.init(client_name, expconfig=expconfig, loglevel=loglevel,
             fileloglevel=logging.DEBUG, listener=True,
             report_status=True, pingpong=True, unique=True)
from _utilities import mqttcleanretained
from daq_queue import GenericDAQQueue

q = GenericDAQQueue(f'imb_{imbid}_que')
decoder = get_decoder(daqname, **dec_kwds)

### generic functions
# set the client status
def _set_status(status: str):

    assert type(status) == str , '"status" is not a string'

    global imbuild_status

    imbuild_status = status
    libABCD.publish(f'img_builders/{imbid}/status', status, retain=True)

def shutdown():
    global bShutdown
    logger.info('Shutting down upon user request.')
    bShutdown = True

# set and publish the current command to MQTT
def set_current_cmd(cmd: str or None):

    global current_cmd
    current_cmd = cmd

    libABCD.publish(f'img_builders/{imbid}/current', cmd, retain=True)

# publish the queue
def publish_queue():
    libABCD.publish(f'img_builders/{imbid}/queue', list(q.deq), retain=True)

# execute a command
def exec_cmd(cmd: str):
    
    set_current_cmd(cmd)
    logger.info(f'Executing: {cmd}')
    _set_status('ExecutingCmd')

    try:
        exec(cmd)
       
    except Exception as e:
        logger.error(f'Could not execute following command: {cmd}. Error: {e}.')

    _set_status('Idle')
    set_current_cmd(None)

# stop a command
def stop():
    logger.info('Received a "stop" signal.')
    # XXX implement

# pause and unpause the queue
def pause_queue():
    global bPause
    if bPause: return
    logger.info('Pausing the queue')
    bPause = True

def unpause_queue():
    global bPause
    if not bPause: return
    logger.info('Unpausing the queue')
    bPause = False

### libABCD callbacks
# handle a "decode" request
def decode_callback(msg: dict, topic: str):

    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    # parse payload
    pmsg = msg['payload']

    if type(pmsg) != str:
        logger.warning(f'Ignoring payload: "{pmsg}". Wrong type.')
        return

    if pmsg == 'shutdown': fun = shutdown
    elif pmsg == 'stop': fun = stop
    elif pmsg == 'pause': fun = pause_queue
    elif pmsg == 'unpause': fun = unpause_queue
    else: 
        q.add(pmsg)
        publish_queue()
        return

    try: fun()
    except Exception as e:
        logger.error(f'Could not execute function. Error: {e}.')


# handle a "image finished" message
def imgfin_callback(msg: dict, topic: str):

    # acknowledge
    logger.debug(f'Received message "{msg}" on topic "{topic}"')

    # parse payload
    msgdaq = topic.partition('/')[2]
    pmsg = msg['payload']

    if daqname == msgdaq:
        s = ''
        for key, val in pmsg.items():
            if type(val) == str:
                val = f'"{val}"'
            s += f'{key}={val},'
        q.add(f'decoder({s})')
        publish_queue()
    
    else:
        logger.debug('Ignored message, not my daq type')


libABCD.add_callback(f'img_builders/{imbid}/inputs', decode_callback)
libABCD.add_callback(f'ccdoutput/+', imgfin_callback) # XXX define the callback in the LDAQ


### startup actions
t.start()
_set_status('Idle')
logger.info(f'image_builder {client_name} initialized.')


### loop
try:
    while not bShutdown:
        
        if len(q.deq)>0:

            if not bPause:
                cmd = q.deq.popleft()
                logger.info(f"Returning next command from queue: {cmd}")
                publish_queue()
                t = threading.Thread(target=exec_cmd, args=(cmd,),
                                        daemon=True)
                t.start()
        
        t.join()
        time.sleep(.1)

        
except KeyboardInterrupt:
    logger.info('Shutting down upon user request.')

except Exception as e:
    logger.critical(f'Unexpected error: {e}. Shutting down.')

finally:
    _set_status('Shutdown')
    for topic in ['current', 'status', 'inputs', 'queue']:
        mqttcleanretained(f'img_builders/{imbid}/'+topic)
    libABCD.disconnect()
