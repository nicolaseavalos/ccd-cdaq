"""
Implements the class Daq_queue, useful to process commands
in two queues, one normal and the other rotating.
"""

import libABCD
from collections import deque
import logging, time
from _utilities import mqttcleanretained#, dump2json


# def _dump_dicts():
#     big_dict = {qname:q.__dict__ for qname,q in RunDAQQueue.active.items()}
#     dump2json(big_dict, 'queue_manager.json')


### generic functions
class GenericDAQQueue:

    def __init__(self, name: str):

        self.name = name
        self.deq = deque()
        self.logger = logging.getLogger(libABCD.name+'.'+name)


    def add(self, data: str):

        self.logger.info(f"Adding to {self.name} - data: {data}")

        try:
            self.deq.append(data)

        except Exception as e:
            self.logger.error(f"Error adding to {self.name}: {e}")


    def addleft(self, data: str):

        self.logger.info(f"Adding to left of {self.name} - data: {data}")

        try:
            self.deq.appendleft(data)

        except Exception as e:
            self.logger.error(f"Error adding to left of {self.name}: {e}")


    def clear(self):

        self.logger.info(f"Clearing {self.name}")
        self.deq.clear()


    def delete(self, p: int or slice):

        if type(p) == slice:
            self.logger.info(f"Deleting elements {p.start} to {p.end} of {self.name}")
        elif type(p) == int:
            self.logger.info(f"Deleting element {p} of {self.name} (was {self.deq[p]})")
        else:
            self.logger.error(f"'p' should be int or slice, was {type(p)}. Skipping.")
            return

        try:
            del self.deq[p]
        except Exception as e:
            self.logger.error(f"Error deleting element(s) from {self.name}: {e}")


class RunDAQQueue:

    active = {}

    # Initialize queue and repeat list
    def __new__(cls, qname='run'):

        if qname in RunDAQQueue.active:
            return RunDAQQueue.active[qname]

        self = object.__new__(cls)

        self.name = qname
        self.current = None
        self.ctime = time.time()
        self.queue = GenericDAQQueue(name=qname+'.queue')
        self.repeat = GenericDAQQueue(name=qname+'.repeat')
        self.targets = []
        self.bPause = False
        self.pause_message = None
        self.bEmpty = True
        self.logger = logging.getLogger('daq_queue.'+qname)

        self._publish_current()
        self._publish_pause()
        self._publish_queue()
        self._publish_repeat()

        RunDAQQueue.active[qname] = self

        return self

    
    # def __setattr__(self, __name: str, __value) -> None:

    #     object.__setattr__(self, __name, __value)
    #     _dump_dicts()


    # Adding/removing targets
    def add_target(self, name):
        self.logger.info(f'Adding target {name} to {self.name}')
        if name in self.targets:
            self.logger.debug(f'Target already in {self.name}. Ignoring.')
        else:
            self.targets.append(name)
            libABCD.publish(f'queues/{self.name}/targets', self.targets, retain=True)

    
    def remove_target(self, name):
        self.logger.info(f'Removing target {name} from {self.name}')
        try:
            self.targets.remove(name)
            libABCD.publish(f'queues/{self.name}/targets', self.targets, retain=True)
        except ValueError:
            self.logger.error(f'No such target. Ignoring.')


    # Adding elements
    def _check_format(self, cmd):

        if 'to' not in cmd:
            self.logger.error(f'"to" not present in command (was {cmd}). Ignoring.')
            return 1

        if 'cmd' not in cmd:
            self.logger.error(f'"cmd" not present in command (was {cmd}). Ignoring.')
            return 1
        
        return 0


    def add_repeat(self, cmd):

        if not self._check_format(cmd):
            self.repeat.add(cmd)
            self.bEmpty = False
            self._publish_repeat()
        

    def add_queue(self, cmd):

        if not self._check_format(cmd):
            self.queue.add(cmd)
            self.bEmpty = False
            self._publish_queue()


    # Clearing the whole queue(s)
    def clear_queue(self):
        self.queue.clear()
        self._publish_queue()


    def clear_repeat(self):
        self.repeat.clear()
        self._publish_repeat()


    def clearall(self):
        self.clear_queue()
        self.clear_repeat()


    # Deleting specific elements
    def del_queue(self, p):

        self.queue.delete(p)
        self._publish_queue()
        

    def del_repeat(self, p):

        self.repeat.delete(p)
        self._publish_repeat()


    # pausing / unpausing
    def _publish_pause(self):
        libABCD.publish(f'queues/{self.name}/paused', self.bPause, retain=True)
        libABCD.publish(f'queues/{self.name}/pause_message', self.pause_message, retain=True)


    def pause(self, message=None):
        if not self.bPause:
            self.logger.info(f'Pausing queue and repeat. Message: {message}.')
        self.bPause = True
        self.pause_message = message
        self._publish_pause()


    def unpause(self):
        if self.bPause:
            self.logger.info('Unpausing queue and repeat.')

        self.bPause = False
        self.pause_message = None
        self._publish_pause()


    # Processing next command
    def _publish_current(self):
        libABCD.publish(f'queues/{self.name}/current', self.current, retain=True)
        libABCD.publish(f'queues/{self.name}/ctime', self.ctime, retain=True)


    def _publish_queue(self):
        libABCD.publish(f'queues/{self.name}/queue', list(self.queue.deq), retain=True)


    def _publish_repeat(self):
        libABCD.publish(f'queues/{self.name}/repeat', list(self.repeat.deq), retain=True)


    def _check_target(self, cmd):

        if cmd['to'] in self.targets:
            return cmd

        self.logger.warning(f'target "{cmd["to"]}" not in this queue target list. Pausing queue.')
        self.pause(message=f'target "{cmd["to"]}" not in this queue target list.')
        self.reset_current()


    def set_current(self, cmd):

        self.current = cmd
        self.ctime = time.time()
        self._publish_current()


    def return_next(self):

        if self.bPause:
            if self.current is not None:
                self.logger.info(f"Paused queue and repeat, returning None.")
                self.set_current(None)
            return None

        # first process all commands in queue
        if len(self.queue.deq)>0:
            cmd = self.queue.deq.popleft()
            self.logger.info(f"Returning next command from queue: {cmd}")
            self.set_current((cmd, 'queue'))
            self._publish_queue()
            return self._check_target(cmd)

        # then do the repeat(s)
        elif len(self.repeat.deq)>0:
            cmd = self.repeat.deq[0]
            self.repeat.deq.rotate(-1)
            self.logger.info(f"Returning next command from repeat: {cmd}")
            self.set_current((cmd, 'repeat'))
            self._publish_repeat()
            return self._check_target(cmd)

        else:
            if not self.bEmpty:
                self.logger.info(f"Empty queue and repeat, returning None.")
                self.set_current(None)
                self.bEmpty = True
            return None


    # return current command to corresponding queue
    def reset_current(self, new_current=None):

        if self.current == None:
            return
        
        if self.current[1] == 'queue':
            self.queue.addleft(self.current[0])
            self._publish_queue()

        elif self.current[1] == 'repeat':
            self.logger.info('Un-rotating repeat')
            self.repeat.deq.rotate(1)
            self._publish_repeat()

        if new_current:
            self.current = (new_current, 'queue')
        else: self.current = None
        self._publish_current()


    def purge_current(self):

        self.current = None
        self.ctime = time.time()
        self._publish_current()


    # delete this queue
    def delete(self):
        for key in ['current', 'ctime', 'queue', 'repeat', 'targets', 'paused', 'pause_message']:
            mqttcleanretained(f'queues/{self.name}/{key}')
        del RunDAQQueue.active[self.name]