#!/usr/bin/env python3

import libABCD
import argparse, logging


parser = argparse.ArgumentParser(description='Send messages to an image builder.')
parser.add_argument('imbid', help='the name of the target image builder')
parser.add_argument('cmd', help='the command to be sent')
args = parser.parse_args()

libABCD.init('sendccd', expconfig='experiment/default.ini', loglevel=logging.WARNING)
from source._utilities import _send_generic
_send_generic(f'img_builders/{args.imbid}/inputs', args.cmd)
libABCD.disconnect()
