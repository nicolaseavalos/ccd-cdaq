Central DAQ for CCD clients v1.6.2
==================================

This set of python apps are designed to manage all the parts of a central DAQ.
Four apps (the "central DAQ") run continuously in one computer while CCD 
clients can be run in the same or any other computer as long as they have 
network access.


CDAQ installation
-----------------

### Install the mosquitto broker
All communications pass through a mosquitto broker. Only the machine running 
the central DAQ needs to have the mosquitto service installed. Please follow 
the instructions given at 
[the mosquitto download page](https://mosquitto.org/download/).


### mosquitto configuration (optional)
If you have ccd clients in other machines, add to the bottom of
`/etc/mosquitto/mosquitto.conf/` the following lines:

listener 1883 0.0.0.0  
allow_anonymous true


### Ensure `tmux` is installed
It is probably already installed in your linux distribution.


### Download the latest release and untar the python environment

Get the latest release [here](https://gitlab.com/nicolaseavalos/ccd-cdaq/-/releases/permalink/latest)

The python interpreter and external libraries are packaged into an environment
named `cdaq_env`. You need to untar them:

```bash
cd /path/to/ccd-cdaq
tar xzf cdaq_env.tar.gz
```


Test the CDAQ installation
--------------------------

To ensure everything works properly, copy the example configuration, start the
CDAQ and execute the example script:

```bash
# copy the example configuration
cd experiment
cp example.ini default.ini
cp example_ccd_clients.json ccd_clients.json
# start the cdaq
cd ..
./start_daq.sh
# execute the example script in the bottom pane of the tmux window
python scripts/example.py
```

you should see commands moving around in the `printqueue` pane, logs appearing
in the `printlog` pane and stuff changing in the `printccdstatus` pane.

When you are done with the test, delete the run queue, shutdown the test
clients and shutdown the CDAQ:

```bash
# delete the run queue
./delqueue.py
# wait for the clients to finish their work
# ...
# shutdown the test clients
./shutdown_client.py fake1 -f
./shutdown_client.py fake2 -f
# shutdown the CDAQ
./shutdown_daq.py
```

To cleanup the `tmux` session, shut down the remaining terminals with `ctrl-c`,
`ctrl-d` or `exit` as required.


Install the LDAQ (e.g. `libLTA`, `libACM`)
------------------------------------------

> IMPORTANT: if your LDAQ requires external python packages, you need to
install them in the `cdaq_env` environment. To do this, activate it:
> ```bash
> source cdaq_env/bin/activate
> ```
> and then install the packages normally (e.g. `pip install <your_lib>`)

If your LDAQ library is installable, install it following the above procedure.
If not, the LDAQ source code should be copied (or linked) to
`source/libDAQ/<your_LDAQ>`.


Setup the CDAQ for your own experiment
--------------------------------------

### CDAQ configuration

The file `experiment/default.ini` must have the information of the MQTT broker
under the [NETWORK] section. An example configuration can be found at 
`experiment/example.ini`.  
Under the [CDAQ] section there are two options:
- hostname : if given, this has to be the name of the computer that hosts
the central DAQ. Therefore if the CDAQ is attempted to be initialized in 
a wrong computer, an error message will be displayed.
- ccds : if given, it should be the name of a json-formatted file with
information of the ccd clients (see below).


### CCD clients configuration  
The file `example_ccd_clients.json` is an example for a configuration file for
the CCD clients. Each entry consists of the name of the client and a dictionary
with some or all of these entries:
- "daq" (required) : the name of the LDAQ (e.g. pylta, ACM, etc)
- "monitoring" : the name of the MQTT topic of the corresponding 
monitoring system  

If a client is meant to run on a different computer, these fields should be
added:
- "host" : the ip of the client computer
- "user" : the username
- "daqpath" : an absolute path to the ccd-cdaq folder in the client computer


Start the CDAQ
--------------

Start the CDAQ apps by running `./start_daq.sh`. This will start a `tmux`
session with several panes in the main window: one for `printqueue.py`, one for
`printccdstatus.py`, one for `printlog.py` and the last one for user input. All
CDAQ commands should be sent through this window.

If the CDAQ was correctly set up, you should see your CCD clients running.


-------------------------------------------------------------------------

Understanding the CDAQ: Some definitions
----------------------------------------

### Queue
The main idea of the CDAQ is that you can send a list of commands to several 
clients and the software will handle their execution: it will wait until one 
command finishes before executing the next. This is accomplished via a 'queue'.
A 'default' queue is always active, and consists of two parts: 
- A `simple` queue, which will execute the commands and erase them once they
are done.
- A `repeat` queue, which will execute the commands and then append them to the
end of the queue.

The `printqueue` pane will print a nice view of this queue.

### CCD pool(s)
Each CCD client can either be treated as an individual client or as part of a
CCD pool. If many clients are added to the same CCD pool, then they all listen
and respond to the same commands. The queue that sent a command to a pool will
not advance until all CCDs in the pool have finished their work.


CDAQ usage
----------

Full documentation of the CDAQ is under way. Find below the most commonly used
commands.

The following commands can be sent via the command line to operate the CDAQ:

### Add a command to the 'default' queue
`./sendrun.py --to CCDID "CMD"`

Replace the following:
- *`CCDID`* with the name of the target CCD client 
- *`CMD`* with the command that will be sent. Note the "" around *CMD*.

Example:
```bash
./sendrun.py --to fake1 "daq.doOneImage()"
```

### Delete commands from the queue
`./delqueue.py [-t {queue|repeat}] [-n CMD_ID]`

Optional arguments:
- `-t`: the type of the queue to clear. If not present, clear both.
- `-n`: remove a specific command. Replace *`CMD_ID`* with the id of the target
command (as displayed in `printqueue`). If not present, clear the whole queue.

Examples:
```bash
# this will clear everything in both "simple" and "repeat" queues.
./delqueue.py 
# this will clear everything in the "repeat" queue.
./delqueue.py -t repeat 
# this will clear the command #5 in the "simple" queue.
./delqueue.py -t queue -n 5 
```

### Add a CCD client to a CCD pool
`./sendrun.py --to CCDID "listen2pool('CCDPOOL')"`

Replace the following:
- *`CCDID`* with the name of the target CCD client 
- *`CCDPOOL`* with the name of the CCD pool.


### Remove a CCD client from a CCD pool
`./sendccd.py CCDID "listen2pool(None)"`

Replace *`CCDID`* with the name of the target CCD client.


### Sending several commands at once: using scripts
You can send a list of commands using python scripts. Place them under the 
`scripts/` directory. All scripts should start importing `libABCD` (the 
communication protocol for the CDAQ):

```python
import libABCD
```

First initialize the communication with the DAQ:  
`libABCD.init('SCRIPT_NAME')` . 

Replace *`SCRIPT_NAME`* with a nice, descriptive name for your script.

Then import the helper function:  

```python
from .utilities import add2runqueue
```

The signature for this function is:

`add2runqueue(cmd, to, queuename='default', queuetype='queue', verbose=False)`

Finally, do not forget to end your script by calling:  
```python
libABCD.disconnect()
```
or else some messages may be lost before they are sent.

You can find an example script at `scripts/example.py`.


Monitor the CDAQ
----------------

The following commands are automatically run by the `start_daq.sh` script, but
can also be run separately:

### Inspect the run queue
To inspect the queue, run `./printqueue.py [-p] [-v]`  
`-p` is persistance mode (useful e.g. for a shifter window)  
`-v` is verbose mode (prints all commands even if there are more than 20)

### Inspect the status of the CCD clients
`./printccdstatus.py [-p]`  
This provides a list of active CCD clients and their attributes, including
current status and last inputs. `-p` is persistance mode.

### Get all logs printed on screen
`./printlog.py`  
This runs a persistant app that flushes all logs in real time on screen.


Advanced usage
--------------

### Non-CCD clients
The CDAQ can also handle clients that are not CCDs (e.g. a heating/cooling 
device). This allows to add commands to these clients in the run control queue.
To add a non-ccd client to the run control, run in the command line:

`./sendqman.py "add_target(TARGET)"`

Replace *`TARGET`* with the name of the non-ccd client. To remove it:

`./sendqman.py "remove_target(TARGET)"`


### Sending a command to a CCD client outside the run_control queue
> WARNING: do this only if you know what you are doing.

`./sendccd.py CCDID "CMD"`


Questions, requests, feedback
-----------------------------

If you find a bug, want to know more about specific features or have feedback
of any kind, do not hesitate to email me at nicolaseavalos AT gmail DOT com.

----------------------------------------------------------------------