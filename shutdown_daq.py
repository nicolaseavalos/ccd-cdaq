#!/usr/bin/env python3

import os, shlex, subprocess

# main daq apps
apps = ['run_control', 
        'Spy', 
        'mon_status',
        'ccdmanager']

# find if there are any screens matching these names
user = os.getenv('USER')
p = subprocess.run(shlex.split(f'ls /var/run/screen/S-{user}'), 
                   stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                   universal_newlines=True)
screen_list = [ n.partition('.')[2] for n in p.stdout.split() ]

# shutdown apps
for app in apps:
    if app in screen_list:
        rawargs = f'screen -S {app} -X stuff \'^C\''
        cmdargs = shlex.split(rawargs)
        p = subprocess.run(cmdargs)
        print(f'> Sent shutdown to {app}')
    else:
        print(f'> Ignored {app} (not running)')
