#!/usr/bin/env python3

import libABCD
import argparse, logging

### parse command line args
parser = argparse.ArgumentParser(description='Clear a queue.')
parser.add_argument('queue', nargs='*', default=['default'], help='the queue name(s)')
parser.add_argument('-t', '--type', choices=['queue', 'repeat'],
                    help='the queue type')
parser.add_argument('-n', help='the element id or slice to clear')
args = parser.parse_args()

libABCD.init('delqueue', loglevel=logging.WARNING, expconfig='experiment/default.ini')

for queue in args.queue:

    if args.type is None:
        cmd = 'clearall()'

    elif args.n is None:
        cmd = f'clear_{args.type}()'

    else:
        cmd = f'del_{args.type}({args.n})'

    print(f'> Sending "{cmd}" to "run/{queue}/inputs"')
    libABCD.publish(f'run/{queue}/inputs', payload=f'{cmd}')

libABCD.disconnect()
