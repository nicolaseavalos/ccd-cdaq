#!/usr/bin/env python3

import argparse, logging, json, subprocess, shlex, time
import libABCD


# parse command line arguments (config file)
parser = argparse.ArgumentParser(description='Start the CCD clients.')
parser.add_argument('jsonfile', default='experiment/ccd_clients.json', nargs='?',
                    help='the JSON file with client information')
parser.add_argument('--expconfig', default='experiment/default.ini',
                    help='the experiment configuration file')
args = parser.parse_args()


# initialize libABCD to get list of active clients
libABCD.init('start_clients', expconfig=args.expconfig, listener=True, loglevel=logging.WARNING)

active_clients = []

def status_callback(msg: dict, topic: str):

    client_id = topic.rpartition('/')[2]

    if 'ccd_client.' not in client_id: return
    if 'listener' in client_id: return

    if msg['payload'] == 'on':
        active_clients.append(client_id)

    # elif client_id in active_clients:
    #     active_clients.remove(client_id)

libABCD.add_callback('status/#', status_callback)


# wait a bit to get all the clients
time.sleep(.5)

if len(active_clients):
    print('>>> Already active clients: ')
    for c in active_clients: print(c)


# load the user-defined client list
with open(args.jsonfile, 'r') as f:
    client_dict = json.load(f)

# start ccd clients
for cname, cdict in client_dict.items():

    client_name = f'ccd_client.{cname}'

    if client_name in active_clients:
        print(f'> {cname} already initialized.')
        continue

    sshargs = ''
    screenargs = f'screen -dmS {client_name}'
    daqpath = ''

    if 'host' in cdict:
        if 'host' != 'localhost':
            if 'user' not in cdict:
                print(f'ERROR - "user" not defined for client {cname}')
                continue
            sshargs = f'ssh {cdict["user"]}@{cdict["host"]}'
            
            if 'script' not in cdict:
                if 'daqpath' in cdict:
                    daqpath = cdict["daqpath"]
                else:
                    print(f'ERROR - either "daqpath" or "script" should be defined for client {cname}')
                    continue

    if 'script' in cdict:
        execargs = cdict["script"]
        optionargs = ''

    else:
        execargs = 'source/ccd_client.py'
        optionargs = f'--ccdid {cname} '

        for key, value in cdict.items():

            if key in ['user', 'host', 'daqpath']: continue

            if key in ['expconfig',]: 
                optionargs += f'--{key} {daqpath}{value} '
                continue

            if key in ['daq_kwds']:
                optionargs += f"--{key} '{json.dumps(value)}'"
                continue

            optionargs += f'--{key} {value} '

    scmd = f'{sshargs} {screenargs} ./{daqpath}{execargs} {optionargs}'
    print(f'Executing: {scmd}')
    subprocess.Popen(shlex.split(scmd))
