#!/usr/bin/env python3

### imports
import libABCD
import argparse, logging, os, time
from datetime import datetime


# parse cmd line arguments
parser = argparse.ArgumentParser(description='Print run control queues.')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='print the whole queues')
parser.add_argument('-p', '--persist', action='store_true',
                    help='persistance mode')
args = parser.parse_args()

verbose = args.verbose


### global variables
queues_dict = {}


### libABCD callbacks
def _print_queue_generic(queue):

    n_msg = len(queue)
    if n_msg == 0:
        print('Empty!')
        return

    n_max = 20
    if verbose:
        n_max = n_msg

    for i, cmd in enumerate(queue):
        
        if i < n_max:
            print(f'[{i}] {cmd}')
            continue

        if not verbose and n_msg != n_max:
            print(f'... {n_msg-n_max} more command(s) ...')
            break

    if i == n_msg-1:
        return

    print(f'[{n_msg-1}] {queue[-1]}')
        

def _print_queue(queuename):

    queue = queues_dict[queuename]
    now = time.time()

    if queue['paused']:
        print(f'Paused with message "{queue["pause_message"]}"')

    s = f'Currently running: {queue["current"]}'
    if queue['ctime']: s += f' (elapsed: {now-queue["ctime"]:.1f} s)'
    print(s)

    print('On simple queue:')
    _print_queue_generic(queue['queue'])

    print('On repeat queue:')
    _print_queue_generic(queue['repeat'])

    if verbose:
        s = 'Targets: '
        for target in queue['targets']: s += f'{target}, '
        print(s[:-2])


def _track_queue(queuename):
    queues_dict[queuename] = {
        # 'active': None,
        'current': None,
        'ctime': None,
        'queue': None,
        'repeat': None,
        'paused': None,
        'pause_message': None,
        'targets': [],
    }


def save_queue_status(msg: dict, topic: str):

    queuename = topic.partition('/')[2].rpartition('/')[0]
    key = topic.rpartition('/')[2]
    value = msg['payload']
    
    if queuename not in queues_dict: _track_queue(queuename)

    if key in queues_dict[queuename]:
        queues_dict[queuename][key] = value


### Initialize libABCD
libABCD.init('printqueue', listener=True, publisher=False, 
             expconfig='experiment/default.ini', loglevel=logging.WARNING)
libABCD.add_callback('queues/+/#', save_queue_status)

### main loop
try:
    
    time.sleep(.5)
    # n = 0
    while True:

        start_time = time.monotonic()
        s = f'printqueue execution time: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}\n'

        if args.persist:
            os.system('clear')
            s = '\033[H' + s

        print(s)

        i = 0
        for queuename in queues_dict:
            # if not queues_dict[queuename]['active']: continue

            print(f'[{i}] {queuename}:')
            _print_queue(queuename)
            print('')
            i += 1

        if not args.persist: break

        libABCD.unsubscribe('queues/+/#')
        queues_dict = {}
        libABCD.add_callback('queues/+/#', save_queue_status)

        elapsed = time.monotonic() - start_time
        if elapsed < 2: 
            time.sleep(2 - elapsed)
        #     n += 1
        # if n > 5:
        #     os.system('clear')
        #     n = 0

        
except Exception as e:
    print(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    print('Shutting down upon user request.')

finally: 
    libABCD.disconnect()

