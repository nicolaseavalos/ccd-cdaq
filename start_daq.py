#!/usr/bin/env python3

import os, shlex, subprocess, configparser, argparse, socket

# parse command line arguments (config file)
parser = argparse.ArgumentParser(description='Start the DAQ.')
parser.add_argument('--expfile', default='experiment/default.ini', 
                    help='the experiment configuration file')
args = parser.parse_args()

# parse config file if given
host = ccdjson = imbjson = None
config = configparser.ConfigParser()
config.read(args.expfile)
if 'CDAQ' in config:
    if 'hostname' in config['CDAQ']:
        host = config['CDAQ']['hostname']
        if host == '': host = None
    if 'ccds' in config['CDAQ']:
        ccdjson = config['CDAQ']['ccds']
        if ccdjson == '': ccdjson = None
    if 'imbs' in config['CDAQ']:
        imbjson = config['CDAQ']['imbs']
        if imbjson == '': imbjson = None
    
# main daq apps
apps = {'run_control': 'source/run_control.py', 
        'Spy':         'source/S.py --ignore-pings', 
        'mon_status':  'source/mon_status.py',
        'udp':  'source/udp.py',
        'ccdmanager':  'source/ccdmanager.py'}

# see if this is the correct machine
if host:
    if host != socket.gethostname():
        print('ERROR: this is NOT the correct host! DAQ will not start.')
        quit()

# find if there are any screens matching these names
user = os.getenv('USER')
p = subprocess.run(shlex.split(f'ls /var/run/screen/S-{user}'), 
                   stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
                   universal_newlines=True)
screen_list = [ n.partition('.')[2] for n in p.stdout.split() ]

# start apps
print('>>> Attempting to start CDAQ apps...')
for app in apps:
    if app not in screen_list:
        rawargs = f'screen -dmS {app} ./{apps[app]} --expconfig {args.expfile}'
        cmdargs = shlex.split(rawargs)
        print(f'Executing: {rawargs}')
        p = subprocess.Popen(cmdargs)
    else:
        print(f'Ignored {app} (already running)')

if ccdjson:
    print(f'>>> Attempting to start ccd clients ({ccdjson})...')
    subprocess.run(shlex.split(f'./start_clients.py experiment/{ccdjson} --expconfig {args.expfile}'))

if imbjson:
    print(f'>>> Attempting to start image builders ({imbjson})...')
    subprocess.run(shlex.split(f'./start_imgbuilders.py experiment/{imbjson} --expconfig {args.expfile}'))
