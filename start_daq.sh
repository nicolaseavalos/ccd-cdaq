#!/bin/bash

# Session Name
SESSION="CCD-CDAQ"

# Select which command to use: check whether conda is installed
if command -v conda &> /dev/null
then
    function envactivate () {
        tmux send-keys 'conda' 'Space' 'activate' 'Space' './cdaq_env' C-m
    }
else
    function envactivate () {
        tmux send-keys 'source' 'Space' 'cdaq_env/bin/activate' C-m
    }
fi

# don't start a new one if there is another already running
SESSIONEXISTS=$(tmux list-sessions | grep $SESSION)

if [ "$SESSIONEXISTS" = "" ]
then

# start a new tmux session
tmux new-session -d -s $SESSION

# printqueue in top-left pane
envactivate
tmux send-keys './printqueue.py -p' C-m

# Split window into two, vertically: 80%/20% of current window.
tmux split-window -v -p 20

# go back to pane 0 (top).
tmux select-pane -t 0

# Split window into two, horizontally: 50%/50% of current window.
tmux split-window -h -p 50

# printccdstatus in top-right pane
envactivate
tmux send-keys './printlog.py -w -c' C-m

# Split window into two, vertically: 40%/60% of current window.
tmux split-window -v -p 40

# print log in mid-right pane
envactivate
tmux send-keys './printccdstatus.py -p -s' C-m

# start daq and send commands in pane 3 (bottom pane)
tmux select-pane -t 3
envactivate
tmux send-keys './start_daq.py' C-m

fi

# Attach Session, on the Main window
tmux attach-session -t $SESSION:0
