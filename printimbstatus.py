#!/usr/bin/env python3

import libABCD
import argparse, logging, os, time
from datetime import datetime


### parse command line arguments
parser = argparse.ArgumentParser(
            description='show the current status of active image builders')
parser.add_argument('-p', '--persist', action='store_true',
                    help='persist mode')
args = parser.parse_args()
bPersist = args.persist


### global variables
imb_dict = {}


### libabcd callbacks
def save_imb_status(msg: dict, topic: str):

    imbid = topic.partition('/')[2].rpartition('/')[0]
    key = topic.rpartition('/')[2]
    if key == 'inputs': return
    value = msg['payload']

    if imbid not in imb_dict:
        imb_dict[imbid] = {}

    imb_dict[imbid][key] = value


### initialize libabcd
libABCD.init('printimbstatus', listener=True, publisher=False,
             expconfig='experiment/default.ini', loglevel=logging.WARNING)
libABCD.add_callback('img_builders/+/#', save_imb_status)


### loop
try:
    time.sleep(.5)
    n = 0
    while True:

        start_time = time.monotonic()
        s = f'printimbstatus execution time: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}\n'

        if bPersist:
            os.system('clear')
            s = '\033[H' + s

        print(s)

        for i,imbid in enumerate(imb_dict):
            if 'status' not in imb_dict[imbid]:
                continue
            if imb_dict[imbid]['status'] == 'Shutdown':
                imb_dict[imbid]['remove'] = True
                continue
            print(f'[{i}] {imbid}:')
            for key in imb_dict[imbid]:
                print(f'    {key}: {imb_dict[imbid][key]}')
            print('')

        imb_dict = {k:v for (k,v) in imb_dict.items() 
                    if 'remove' not in v}

        if not bPersist: break

        elapsed = time.monotonic() - start_time
        if elapsed < 2: 
            time.sleep(2 - elapsed)
            n += 1
        if n > 5:
            os.system('clear')
            n = 0


except Exception as e:
    print(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    print('Shutting down upon user request.')

finally: 
    libABCD.disconnect()
