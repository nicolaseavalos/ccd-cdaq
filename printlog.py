#!/usr/bin/env python3

import libABCD
import time, logging, os
from datetime import datetime
from termcolor import colored

### parse command line args
import argparse
parser = argparse.ArgumentParser(
                    description='Print all CDAQ logs.')
parser.add_argument('-e','--expconfig', type=str, 
                    default='experiment/default.ini',
                    help='a configuration file for the experiment')
parser.add_argument('-d','--debug', action='store_true', 
                    help='log debug messages')
parser.add_argument('-c','--colors', action='store_true', 
                    help='colors all messages')
parser.add_argument('-w','--warning', action='store_true', 
                    help='show last warning or error message')
args = parser.parse_args()
debug = args.debug
warning = args.warning
colors = args.colors
if debug:
    loglevel = logging.DEBUG
else: loglevel = logging.INFO

expconfig = args.expconfig

warningmsg = None

### main configuration
name = "printlog"

### initialize mqtt and logger
libABCD.init(name, expconfig=expconfig, loglevel=loglevel, listener=True, publisher=False)

def get_log_level_color(level):
    """Get the color for a log level."""
    colors = {
        "INFO": "green",
        "WARNING": "yellow",
        "ERROR": "red"
    }
    return colors.get(level.strip())

def parse(msg,topic):
    logfrom = topic.partition('/')[2]
    logtime = msg['timestamp']
    loglevel = msg['payload']['level']
    logmsg = msg['payload']['msg']
    logdatetime = datetime.fromtimestamp(logtime).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    return logfrom,loglevel,logmsg,logdatetime,logtime

### mqtt callbacks
def on_message_log(msg: dict, topic: str):
    global warningmsg
    try:
        logfrom,loglevel,logmsg,logdatetime,logtime = parse(msg,topic)
        if warning:
            if (loglevel != "INFO") and (loglevel != "DEBUG"):
                warningmsg=[msg,topic]
    except:
        return   # silent if fail
    if logfrom == name: return # do not re-log self logs

    print(" "*os.get_terminal_size().columns,end="\r")
    m=f'{logdatetime} - {logfrom: <15} - {loglevel.upper(): <8} - {logmsg}'
    if colors:
        m=colored(m,color=get_log_level_color(loglevel.upper()))
    print(m)
    if warning and warningmsg:
        logfrom,loglevel,logmsg,logdatetime,logtime = parse(*warningmsg)
        dt=time.time()-logtime
        if dt>500:
            warningmsg=None
        m=f'Last {loglevel.upper()} - {datetime.fromtimestamp(logtime).strftime("%H:%M:%S")} - {logfrom: <15} - {logmsg}'
        m=m[:os.get_terminal_size().columns-5]+"+"
        print(colored(m,color=get_log_level_color(loglevel.upper())), end="\r")



print(f'printlog execution time: {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')

libABCD.add_callback('logs/#', on_message_log)

### loop
try:
    while True:
        time.sleep(1)
    
except Exception as e:
    print(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    print('Shutting down upon user request.')

finally: 
    libABCD.disconnect()
