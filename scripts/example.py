#!/usr/bin/env python3

"""
This example script sends commands to two CCD clients. First, it
requests for each of them to take a single image (one after the other),
simulating a startup action. Then it adds both to a ccdpool and adds a
`repeat` command requesting to take an image for eternity.
"""

import libABCD

# initialize communication
libABCD.init('example_script')
from _utilities import add2runqueue

# this wrapper simplifies the commands if you are sending 
# mostly to one ccd client
def Send(cmd, to='fake1', qtype='queue'):
    add2runqueue(cmd, to=to, queuetype=qtype, verbose=True)

# tell fake1 to take 2 images
for i in range(2):
    Send('daq.doOneImage(wait=10)')

# tell fake2 to take 3 images
for i in range(3):
    Send('daq.doOneImage(wait=5)', to='fake2')

# add both to the same pool
Send("listen2pool('ccdpool')", to='fake1')
Send("listen2pool('ccdpool')", to='fake2')

# send repeating commands
Send('daq.doOneImage(wait=.1)', to='ccdpool', qtype='repeat')
Send('daq.doOneImage(wait=20)', to='ccdpool', qtype='repeat')
Send('daq.doOneImage(wait=.1)', to='fake1', qtype='repeat')
Send("daq.doOneImage(wait=.3)", to='fake2', qtype='repeat')

# end the communication
libABCD.disconnect()
