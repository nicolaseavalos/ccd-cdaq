#!/usr/bin/env python3

import libABCD
import argparse, json, logging, os, time
from datetime import datetime


### parse command line arguments
parser = argparse.ArgumentParser(
            description='show the current status of active ccd clients')
parser.add_argument('-s', '--summary', action='store_true',
                    help='summary mode')
parser.add_argument('-p', '--persist', action='store_true',
                    help='persist mode')
args = parser.parse_args()
bPersist = args.persist
bSummary = args.summary


### global variables
ccd_client_dict = {}
ccd_pool_dict = {}

name = 'printccdstatus'

### libabcd callbacks
def save_ccd_status(msg: dict, topic: str):

    ctype = topic.partition('/')[0]
    ccdid = topic.partition('/')[2].rpartition('/')[0]

    if ctype == 'ccd_clients': ccd_dict = ccd_client_dict
    if ctype == 'ccd_pools': ccd_dict = ccd_client_dict

    if msg is None:
        try:
            del ccd_dict[ccdid]
        except KeyError: pass
        return

    key = topic.rpartition('/')[2]
    if key not in ['status', 'daq', 'monitoring', 'current', 'pool', 'clients', 'ctime']: return
    value = msg['payload']

    if ccdid not in ccd_dict:
        ccd_dict[ccdid] = {}

    ccd_dict[ccdid][key] = value


def save_ccdpool_status(msg, topic):

    if msg is None:
        try:
            del ccd_pool_dict[ccdid]
        except KeyError: pass
        return




def shutdown():
    libABCD.disconnect()


### initialize libabcd
libABCD.init(name, listener=True, publisher=False,
            expconfig='experiment/default.ini', loglevel=logging.WARNING)
libABCD.add_callback('ccd_clients/+/#', save_ccd_status, 
                        allow_empty_payload=True)

def pretty_print_duration(total_seconds):
    hours = total_seconds // 3600
    minutes = (total_seconds % 3600) // 60
    seconds = total_seconds % 60

    parts = []
    if hours > 0:
        parts.append(f"{int(hours)}h")
    if minutes > 0:
        parts.append(f"{int(minutes)}min")
    if seconds > 0 or len(parts) == 0:  # Always show seconds if no hours or minutes
        parts.append(f"{seconds:.1f}sec")

    return ' '.join(parts)

def refresh_screen():

    s = f'Client status on {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'

    if bPersist:
        os.system('clear')
        s = '\033[H' + s

    print(s)
    if not bSummary:
        print("")


def print_client_list(ccd_dict):

    for i,(ccdid, cdict) in enumerate(ccd_dict.items()):

        if bSummary:
            c=None
            if "clients" in cdict:
                c=cdict["clients"]
            if "daq" in cdict:
                c="("+cdict["daq"]+")"
            if not c:
                continue
            print(f'[{i}] {ccdid} {c}: [{cdict["status"][0:4]}] {cdict["current"]}',end="")
            if 'ctime' in cdict:
                dtime = datetime.fromtimestamp(cdict["ctime"])
                elapsed = datetime.now() - dtime
                print(f' ({pretty_print_duration(elapsed.total_seconds())})')
            else:
                print("")
            continue
        print(f'[{i}] {ccdid}:')

        for key in ['daq', 'pool', 'clients', 'status', 'current']:
            if key in cdict:
                print(f'    {key}: {cdict[key]}')
        
        if 'ctime' in cdict:
            dtime = datetime.fromtimestamp(cdict["ctime"])
            elapsed = datetime.now() - dtime
            print(f'    ctime: {dtime.strftime("%Y-%m-%d %H:%M:%S")} ({elapsed.total_seconds():.1f}s ago)')

        print('')


### loop
try:
    time.sleep(.5)
    # n = 0
    while True:

        start_time = time.monotonic()
        refresh_screen()

        print('### CCD CLIENTS ###')
        print_client_list(ccd_client_dict)

        if len(ccd_pool_dict):
            print('### CCD POOLS ###')
            print_client_list(ccd_pool_dict)

        if not bPersist: break

        elapsed = time.monotonic() - start_time
        if elapsed < 2: 
            time.sleep(2 - elapsed)
        #     n += 1
        # if n > 5:
        #     os.system('clear')
        #     n = 0
        

except Exception as e:
    print(f'Unexpected error: {e}. Shutting down.')

except KeyboardInterrupt:
    print('Shutting down upon user request.')

finally:
    shutdown()
