#!/usr/bin/env python3

import libABCD    
import argparse, logging

parser = argparse.ArgumentParser(description='Manage queues.')
parser.add_argument('message', default=None, type=str,
                    help='the command to be sent')
parser.add_argument('--to', type=str, default='default', 
                    help='the target of the message')
args = parser.parse_args()

libABCD.init('sendqman', expconfig='experiment/default.ini', loglevel=logging.WARNING)
from source._utilities import send2queuemanager
send2queuemanager(args.message, qname=args.to, verbose=True)
libABCD.disconnect()
